import axios from 'axios';

const state = {
  employees: {
    jobTitle: '',
    mobilePhone: '',
    officeLocation: '',
  },
  userName: '',
  isSeen: true
};

const getters = {
  allEmployee: (state: any) => state.employees,
  userName: (state: any) => state.userName,
  isSeen: (state: any) => state.isSeen
};

const actions = {
  async loadEmployees({ commit }: any) {
    const response = await axios.get(
      'http://demo9838616.mockable.io/spr-mock-person-details',
    );

    commit('SET_EMPLOYEES', response.data);
  },
  loadUserName({ commit }: any, name: any) {
    commit('SET_USER_NAME', name);
  },
  changeVisibility({ commit }: any, visibility: any) {
    const newVisibility = !visibility;

    if (newVisibility === true) {
      commit('SET_USER_NAME','');
    }
    
    commit('SET_VISIBILITY', newVisibility);
  },
};

const mutations = {
  SET_EMPLOYEES(state: any, employees: any) {
    state.employees = employees;
  },
  SET_USER_NAME: (state: any, userName: any) => state.userName = userName,
  SET_VISIBILITY: (state: any, visibility: any) => state.isSeen = visibility
};

export default {
  state,
  getters,
  mutations,
  actions,
};
