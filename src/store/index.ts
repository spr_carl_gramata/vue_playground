import Vuex from 'vuex';
import Vue from 'vue';
import store from '@/store/modules/store';
import createPersistedState from 'vuex-persistedstate'

// load Vuex
Vue.use(Vuex);

// create store
export default new Vuex.Store({
  plugins: [createPersistedState()],
  modules: {
    store
  },
});
